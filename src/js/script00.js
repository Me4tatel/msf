var header = document.querySelector("header");

// body paddinTop and header add shadow
function bodyPaddingTop() {
    var headerHeight = header.clientHeight;
    var bodyPadding = headerHeight + 50;
    document.body.style.paddingTop = bodyPadding + "px";

    if (
        document.body.scrollTop > headerHeight ||
        document.documentElement.scrollTop > headerHeight
    ) {
        header.classList.add("shadow");
    } else {
        header.classList.remove("shadow");
    }
}

bodyPaddingTop();

// .open/.show mobile menu
document.querySelector(".mobile_button").addEventListener("click", function(e) {
    console.log(window.innerWidth);
    if (window.innerWidth <= 992) {
        document.body.classList.toggle("hidden");
        e.target.closest("header").classList.toggle("active");
    }
});

// header shadow
window.addEventListener("scroll", function(e) {
    bodyPaddingTop();
});

// window resize => hide mobile menu
window.addEventListener("resize", function() {
    bodyPaddingTop();
    if (window.innerWidth >= 992) {
        if (header.classList.contains("active")) {
            header.classList.remove("active");
        }
        if (document.body.classList.contains("hidden")) {
            document.body.classList.remove("hidden");
        }
    }
    checkResize();
});

select(".head_tab .list", function(el) {
    el.addEventListener("click", function(e) {
        this.classList.add("active");
        var currentIndex = this.myIndex;
        var wrap = e.target.closest(".container");

        select(
            ".head_tab .list",
            function(el) {
                if (el.myIndex !== currentIndex) {
                    el.classList.remove("active");
                }
            },
            wrap
        );

        select(
            ".body_maps .map",
            function(el) {
                if (el.myIndex === currentIndex) {
                    el.classList.add("active");
                } else {
                    el.classList.remove("active");
                }
            },
            wrap
        );
    });
});

function select(name, callback, parentNode) {
    var el = null;
    if (parentNode) {
        el = parentNode.querySelectorAll(name);
    } else {
        el = document.querySelectorAll(name);
    }

    if (el) {
        el.forEach(function(subEl, index) {
            subEl.myIndex = index;
            callback(subEl);
        });
    } else {
        console.error("Element not found!");
    }
}

(function() {
    if (!Element.prototype.closest) {
        Element.prototype.closest = function(css) {
            var node = this;
            while (node) {
                if (node.matches(css)) return node;
                else node = node.parentElement;
            }
            return null;
        };
    }
    if (!Element.prototype.matches) {

        // определяем свойство
        Element.prototype.matches = Element.prototype.matchesSelector ||
            Element.prototype.webkitMatchesSelector ||
            Element.prototype.mozMatchesSelector ||
            Element.prototype.msMatchesSelector;

    }
})();
var getSiblings = function (elem) {

    // Setup siblings array and get the first sibling
    var siblings = [];
    var sibling = elem.parentNode.firstChild;

    // Loop through each sibling and push to the array
    while (sibling) {
        if (sibling.nodeType === 1 && sibling !== elem) {
            siblings.push(sibling);
        }
        sibling = sibling.nextSibling
    }

    return siblings;

};
function myFunction() {
  var element, name, arr;
  element = document.getElementById("myDIV");
  name = "mystyle";
  arr = element.className.split(" ");
  if (arr.indexOf(name) == -1) {
    element.className += " " + name;
  }
}   

function checkResize(){
    var parents = document.getElementsByClassName('faq-blocks');
    for (var i = 0; i < parents.length; i++) {
        contents = parents[i].getElementsByClassName('faq-block-content');
        for (var j = 0; j < contents.length; j++) {
            contents[j].style.maxHeight = contents[j].getElementsByClassName('inner')[0].offsetHeight + 'px';
        }
    }
}
window.onload = function(){
    checkResize();
}
for (var i = 0; i < document.getElementsByClassName('faq-block-title').length; i++) {
    document.getElementsByClassName('faq-block-title')[i].addEventListener('click', function() {
        var element = this.closest('.faq-block');
        if (element.className.indexOf('show') !== -1) {
            element.className = element.className.replace(/\bshow\b/g, "");
        } else {
            getSiblings(element).forEach(function(item){
                item.className = item.className.replace(/\bshow\b/g, "");
            });
            element.className += " show";
        }
    })
}

if(document.getElementsByClassName('icon-search').length){
    document.getElementsByClassName('icon-search')[0].addEventListener('click', function(e) {
        var element = this.closest('.wrapp-search');
        if (element.className.indexOf('show') === -1 && window.innerWidth > 992) {
            e.preventDefault();
            element.className += " show";
        }
    })
}

document.addEventListener('click', function(event) {
    if (!event.target.closest('.wrapp-search') && document.getElementsByClassName('wrapp-search').length) {
        document.getElementsByClassName('wrapp-search')[0].className = document.getElementsByClassName('wrapp-search')[0].className.replace(/\bshow\b/g, "");
    }
});
